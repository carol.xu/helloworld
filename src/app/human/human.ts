export class Human {

  constructor(
    public firstname: string,
    public lastname: string,
    public favFruit: string,
    public hairColor: string,
    public height?: number,
    public weight?: number
  ) { }
}