import { Component } from '@angular/core';

import { Human } from './human';

@Component({
  selector: 'app-human',
  templateUrl: './human.component.html',
  styleUrls: ['./human.component.scss']
})
export class HumanComponent {

  fruit = ['Apple', 'Orange', 'Banana', 'Lime',
    'Blueberry', 'Grape', 'Pomegranate', 'Mango'];

  model = new Human('John', 'Doe', this.fruit[0], 'Brown');
  submitted = false;
  onSubmit() { this.submitted = true; }

  resetHuman() {
    this.model = new Human('John', 'Doe', this.fruit[0], 'Brown');
  }

  resetToForm() {
    this.submitted = false;
  }

}
